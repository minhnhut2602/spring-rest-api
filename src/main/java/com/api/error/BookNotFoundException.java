package com.api.error;

public class BookNotFoundException extends RuntimeException {

    /**
	 * 
	 */
	private static final long serialVersionUID = -2142075224355040548L;

	public BookNotFoundException(Long id) {
        super("Book id not found : " + id);
    }

}