package com.api.model;

import org.springframework.stereotype.Repository;

@Repository
public class EmployeeDAO 
{
    private static Employees list = new Employees();
    
    static 
    {
        list.getEmployeeList().add(new Employee(1, "abc", "bcd", "abc@gmail.com"));
        list.getEmployeeList().add(new Employee(2, "asd", "zxc", "asd@gmail.com"));
        list.getEmployeeList().add(new Employee(3, "qwe", "wer", "qwe@gmail.com"));
    }
    
    public Employees getAllEmployees() 
    {
        return list;
    }
    
    public void addEmployee(Employee employee) {
        list.getEmployeeList().add(employee);
    }
}
