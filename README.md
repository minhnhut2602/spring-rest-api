# Spring REST API

## 1. How to start
```
$ cd spring-rest-hello-world
$ mvn spring-boot:run
$ curl -v localhost:8080/books
$ curl -v localhost:8080/books/1
$ curl -v -X POST localhost:8080/books -H "Content-type:application/json" -d "{\"name\":\"Spring REST tutorials\",\"author\":\"mkyong\",\"price\":\"9.99\"}"
$ curl -v -X PUT localhost:8080/books/4 -H "Content-type:application/json" -d "{\"name\":\"Spring Forever\",\"author\":\"pivotal\",\"price\":\"9.99\"}"
$ curl -v -X PATCH localhost:8080/books/4 -H "Content-type:application/json" -d "{\"author\":\"oracle\"}"
$ curl -v -X DELETE localhost:8080/books/4
```